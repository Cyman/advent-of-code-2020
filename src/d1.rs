use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day1.txt").unwrap();
    let reader = BufReader::new(file);

    let numbers: Vec<u32> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap().parse::<u32>().unwrap())
        .collect();

    'a: for number in numbers.iter() {
        for number2 in numbers.iter() {
            if number + number2 == 2020 {
                println!("Part 1: {}", number * number2);
                break 'a;
            }
        }
    }

    'b: for number in numbers.iter() {
        for number2 in numbers.iter() {
            for number3 in numbers.iter() {
                if number + number2 + number3 == 2020 {
                    println!("Part 2: {}", number * number2 * number3);
                    break 'b;
                }
            }
        }
    }
}


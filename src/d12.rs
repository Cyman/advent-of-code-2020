use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day12.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<(String, isize)> = reader
        .lines()
        .into_iter()
        .map(|value| {
            let mut direction = value.unwrap();
            let value = direction.split_off(1).parse::<isize>().unwrap();
            (direction, value)
        })
        .collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

fn part_1(lines: &Vec<(String, isize)>) -> isize {
    //north is 0 degrees, east is 90 degrees, south is 180 degrees, west is 270 degrees
    let mut current_direction: isize = 90;
    let mut x_location: isize = 0;
    let mut y_location: isize = 0;

    for (direction, value) in lines.iter() {
        match direction.as_str() {
            "E" => x_location += value,
            "S" => y_location -= value,
            "W" => x_location -= value,
            "N" => y_location += value,
            "L" => {
                if current_direction - value < 0 {
                    current_direction = 360 + (current_direction - value);
                } else {
                    current_direction -= value;
                }
            }
            "R" => current_direction = (current_direction + value) % 360,
            "F" => {
                match current_direction {
                    0 => y_location += value,
                    90 => x_location += value,
                    180 => y_location -= value,
                    270 => x_location -= value,
                    _ => panic!("Wrong direction!")
                }
            }
            _ => panic!("Wrong direction!"),
        }
    }

    x_location.abs() + y_location.abs()
}

fn part_2(lines: &Vec<(String, isize)>) -> isize {
    //north is 0 degrees, east is 90 degrees, south is 180 degrees, west is 270 degrees
    let mut ship_x_location: isize = 0;
    let mut ship_y_location: isize = 0;
    let mut waypoint_x_location: isize = 10;
    let mut waypoint_y_location: isize = 1;

    for (direction, value) in lines.iter() {
        match direction.as_str() {
            "E" => waypoint_x_location += value,
            "S" => waypoint_y_location -= value,
            "W" => waypoint_x_location -= value,
            "N" => waypoint_y_location += value,
            "L" => {
                match value {
                    90 => {
                        waypoint_y_location *= -1;
                        std::mem::swap(&mut waypoint_x_location, &mut waypoint_y_location); 
                    },
                    180 => {
                        waypoint_x_location *= -1;
                        waypoint_y_location *= -1;
                    },
                    270 => {
                        waypoint_x_location *= -1;
                        std::mem::swap(&mut waypoint_x_location, &mut waypoint_y_location); 
                    },
                    _ => panic!("Wrong direction!")
                }
            }
            "R" => {
                match value {
                    90 => {
                        waypoint_x_location *= -1;
                        std::mem::swap(&mut waypoint_x_location, &mut waypoint_y_location); 
                    },
                    180 => {
                        waypoint_x_location *= -1;
                        waypoint_y_location *= -1;
                    },
                    270 => {
                        waypoint_y_location *= -1;
                        std::mem::swap(&mut waypoint_x_location, &mut waypoint_y_location); 
                    },
                    _ => panic!("Wrong direction!")
                }
            },
            "F" => {
                ship_x_location += waypoint_x_location * value;
                ship_y_location += waypoint_y_location * value;
            }
            _ => panic!("Wrong direction!"),
        }
    }

    ship_x_location.abs() + ship_y_location.abs()
}

use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
struct InputData {
    input_rules: HashMap<usize, InputRule>,
    messages: Vec<String>,
}

#[derive(Debug, Clone)]
struct InputRule {
    rule_number: usize,
    character: Option<String>,
    numbers: Vec<usize>,
    alt_numbers: Vec<usize>,
}

fn main() {
    let file = File::open("src/input/day19.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader.lines().into_iter().map(|x| x.unwrap()).collect();
    let input_data: InputData = parse_input(&lines);

    println!("Part 1: {}", part_1(&input_data));
}

fn part_1(input_data: &InputData) -> usize {
    let combinations = get_combinations_for_rule(&input_data.input_rules, 0);
    let mut count: usize = 0;

    for message in &input_data.messages {
        if combinations.contains(message) {
            count += 1;
        }
    }

    count
}

fn get_combinations_for_rule(
    rules: &HashMap<usize, InputRule>,
    rule_number: usize
) -> Vec<String> {
    let rule: &InputRule = rules.get(&rule_number).unwrap();

    if rule.character.is_some() {
        return vec![rule.character.clone().unwrap()];
    }

    let mut result_combinations: Vec<String> = Vec::new();
    let mut string_combinations: Vec<Vec<String>> = Vec::new();

    for number in &rule.numbers {
        string_combinations.push(get_combinations_for_rule(
            &rules,
            *number
        ));
    }

    let strings = get_strings_from_combinations(string_combinations);

    for s in strings {
        result_combinations.push(s);
    }

    string_combinations = Vec::new();

    for number in &rule.alt_numbers {
        string_combinations.push(get_combinations_for_rule(
            &rules,
            *number
        ));
    }

    let strings = get_strings_from_combinations(string_combinations);

    for s in strings {
        result_combinations.push(s);
    }

    result_combinations
}

fn get_strings_from_combinations(combinations: Vec<Vec<String>>) -> Vec<String> {
    let mut strings: Vec<String> = Vec::new();
    let mut tmp_strings: Vec<String> = Vec::new();

    if combinations.len() == 0 {
        return Vec::new();
    }

    for string in &combinations[0] {
        strings.push(string.clone());
    }

    for (id, combination) in combinations.iter().enumerate() {
        if id == 0 {
            continue;
        }

        for string in &strings {
            for letter in combination {
                tmp_strings.push(format!("{}{}", string, letter));
            }
        }

        strings = tmp_strings;
        tmp_strings = Vec::new();
    }

    strings
}

fn parse_input(lines: &Vec<String>) -> InputData {
    let mut line_pointer: usize = 0;
    let mut input_rules: HashMap<usize, InputRule> = HashMap::new();

    loop {
        let split_at_colon: Vec<&str> = lines[line_pointer].split(": ").collect();
        let rule_number: usize = split_at_colon[0].to_owned().parse::<usize>().unwrap();
        let mut numbers: Vec<usize> = Vec::new();
        let mut alt_numbers: Vec<usize> = Vec::new();
        let mut character: Option<String> = None;

        if split_at_colon[1].chars().nth(0).unwrap().to_string() == "\"" {
            character = Some(split_at_colon[1].chars().nth(1).unwrap().to_string());
        }

        if character.is_none() {
            if split_at_colon[1].contains("|") {
                let split_at_pipe: Vec<&str> = split_at_colon[1].split(" | ").collect();
                numbers = split_at_pipe[0]
                    .split(" ")
                    .map(|x| x.to_owned().parse::<usize>().unwrap())
                    .collect();
                alt_numbers = split_at_pipe[1]
                    .split(" ")
                    .map(|x| x.to_owned().parse::<usize>().unwrap())
                    .collect();
            } else {
                numbers = split_at_colon[1]
                    .split(" ")
                    .map(|x| x.to_owned().parse::<usize>().unwrap())
                    .collect();
            }
        }

        input_rules.insert(
            rule_number,
            InputRule {
                rule_number,
                character,
                numbers,
                alt_numbers,
            },
        );

        line_pointer += 1;
        if lines[line_pointer] == "" {
            break;
        }
    }

    line_pointer += 1;
    let mut messages: Vec<String> = Vec::new();

    loop {
        messages.push(lines[line_pointer].clone());

        line_pointer += 1;
        if line_pointer == lines.len() {
            break;
        }
    }

    InputData {
        input_rules,
        messages,
    }
}

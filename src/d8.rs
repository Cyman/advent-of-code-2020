use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};

struct Computer {
    acc: isize,
    pointer: usize,
    already_executed_pointers: HashSet<usize>,
    instructions: Vec<String>,
}

enum Errors {
    InfiniteLoop,
}

impl Computer {
    fn new(instructions: Vec<String>) -> Self {
        Computer {
            acc: 0,
            pointer: 0,
            already_executed_pointers: HashSet::new(),
            instructions,
        }
    }

    fn start_execution(&mut self) -> Result<isize, Errors> {
        loop {
            //check for infinite loop
            if self.already_executed_pointers.contains(&self.pointer) {
                return Err(Errors::InfiniteLoop);
            } else {
                self.already_executed_pointers.insert(self.pointer);
            }

            //check for end of program
            if self.pointer > self.instructions.len() - 1 {
                return Ok(0);
            }

            //parse instruction
            let instruction = self.instructions[self.pointer].clone();
            let split: Vec<&str> = instruction.split(' ').collect();
            let operation = split[0];
            let argument = split[1].to_owned().parse::<isize>().unwrap();

            //execute instruction
            match operation {
                "nop" => self.pointer += 1,
                "acc" => {
                    self.acc += argument;
                    self.pointer += 1;
                }
                "jmp" => {
                    self.pointer = (self.pointer as isize + argument) as usize;
                }
                _ => panic!("Unknown operation!"),
            }
        }
    }
}

fn main() {
    let file = File::open("src/input/day8.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap())
        .collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

fn part_1(lines: &Vec<String>) -> isize {
    let lines_clone = lines.clone();
    let mut computer = Computer::new(lines_clone);
    let _ = computer.start_execution();
    computer.acc
}

fn part_2(lines: &Vec<String>) -> isize {
    for (id, line) in lines.iter().enumerate() {
        let mut lines_clone = lines.clone();

        if line.contains("jmp") {
            lines_clone[id] = lines_clone[id].replace("jmp", "nop");
        }

        if line.contains("nop") {
            lines_clone[id] = lines_clone[id].replace("nop", "jmp");
        }

        if !line.contains("acc") {
            let mut computer = Computer::new(lines_clone);
            let result = computer.start_execution();

            if result.is_ok() {
                return computer.acc;
            }
        }
    }

    0
}

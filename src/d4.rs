use regex::Regex;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Clone, Debug)]
struct Passport {
    byr: Option<String>,
    iyr: Option<String>,
    eyr: Option<String>,
    hgt: Option<String>,
    hcl: Option<String>,
    ecl: Option<String>,
    pid: Option<String>,
    cid: Option<String>,
}

impl Passport {
    fn new() -> Self {
        Passport {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: None,
        }
    }
}

fn part_1(passports: &Vec<Passport>) -> u32 {
    let mut correct_passports = 0;

    for passport in passports.iter() {
        if passport.byr.is_some()
            && passport.iyr.is_some()
            && passport.eyr.is_some()
            && passport.hgt.is_some()
            && passport.hcl.is_some()
            && passport.ecl.is_some()
            && passport.pid.is_some()
        {
            correct_passports += 1;
        }
    }

    correct_passports
}

fn part_2(passports: &Vec<Passport>) -> u32 {
    let mut correct_passports = 0;

    for passport in passports.iter() {
        if !exists_numeric_between_values(&passport.byr, 1920, 2002)
            || !exists_numeric_between_values(&passport.iyr, 2010, 2020)
            || !exists_numeric_between_values(&passport.eyr, 2020, 2030)
            || !exists_matches_regex(
                &passport.hcl,
                "^#([0-9]|[a-f]){6}$",
            )
            || !exists_matches_regex(
                &passport.ecl,
                "^(amb|blu|brn|gry|grn|hzl|oth)$",
            )
            || !exists_matches_regex(
                &passport.pid,
                "^\\d{9}$",
            )
        {
            continue;
        }

        match &passport.hgt {
            Some(existing) => match &existing[existing.len() - 2..] {
                "cm" => match existing[..existing.len() - 2].to_string().parse::<u32>() {
                    Ok(numeric) => {
                        if numeric < 150 || numeric > 193 {
                            continue;
                        }
                    }
                    _ => continue,
                },
                "in" => match existing[..existing.len() - 2].to_string().parse::<u32>() {
                    Ok(numeric) => {
                        if numeric < 59 || numeric > 76 {
                            continue;
                        }
                    }
                    _ => continue,
                },
                _ => continue,
            },
            None => continue,
        }

        correct_passports += 1;
    }

    correct_passports
}

fn exists_numeric_between_values(value: &Option<String>, min: u32, max: u32) -> bool {
    match value {
        Some(existing) => match existing.parse::<u32>() {
            Ok(numeric) => numeric >= min && numeric <= max,
            _ => false,
        },
        None => false,
    }
}

fn exists_matches_regex(value: &Option<String>, regex: &str) -> bool {
    match value {
        Some(existing) => {
            let regex = Regex::new(regex).unwrap();
            regex.is_match(existing)
        }
        None => false,
    }
}

fn main() {
    let file = File::open("src/input/day4.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap())
        .collect();

    let mut passports: Vec<Passport> = Vec::new();
    let mut passport = Passport::new();

    for line in lines.iter() {
        if line != "" {
            let elements: Vec<&str> = line.split(' ').collect();

            for element in elements.iter() {
                let string_element = String::from(*element);
                let data: Vec<&str> = string_element.split(':').collect();
                let label: &str = data[0];
                let value: &str = data[1];

                match label {
                    "byr" => passport.byr = Some(String::from(value)),
                    "iyr" => passport.iyr = Some(String::from(value)),
                    "eyr" => passport.eyr = Some(String::from(value)),
                    "hgt" => passport.hgt = Some(String::from(value)),
                    "hcl" => passport.hcl = Some(String::from(value)),
                    "ecl" => passport.ecl = Some(String::from(value)),
                    "pid" => passport.pid = Some(String::from(value)),
                    "cid" => passport.cid = Some(String::from(value)),
                    _ => panic!("Wrong label!"),
                }
            }
        } else {
            passports.push(passport.clone());
            passport = Passport::new();
        }
    }

    passports.push(passport.clone());

    println!("Part 1: {}", part_1(&passports));
    println!("Part 2: {}", part_2(&passports));
}

use std::fs::File;
use std::io::{BufRead, BufReader};

fn calculate_trees(lines: &Vec<String>, right: usize, down: usize) -> u64 {
    let mut tree_count = 0;

    for (y, line) in lines.iter().enumerate() {
        let x = (y as f32 * (right as f32 / down as f32)) as usize;

        if y % down == 0 && line.chars().nth(x).unwrap() == '#' {
            tree_count += 1;
        }
    }

    tree_count
}

fn part_1(lines: &Vec<String>) -> u64 {
    calculate_trees(&lines, 3, 1)
}

fn part_2(lines: &Vec<String>) -> u64 {
    calculate_trees(&lines, 1, 1)
        * calculate_trees(&lines, 3, 1)
        * calculate_trees(&lines, 5, 1)
        * calculate_trees(&lines, 7, 1)
        * calculate_trees(&lines, 1, 2)
}

fn main() {
    let file = File::open("src/input/day3.txt").unwrap();
    let reader = BufReader::new(file);

    let mut lines: Vec<String> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap())
        .collect();
    let minimum_x_length = lines.iter().count() * 7;
    lines = lines
        .into_iter()
        .map(|value| {
            let line_length = value.chars().into_iter().count();
            let repeat_count = minimum_x_length / line_length;
            let mut line = value.clone();

            for _ in 0..repeat_count {
                line += &value.to_owned();
            }

            line
        })
        .collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str::FromStr;

#[derive(Debug)]
struct PasswordRecord {
    min_count: u32,
    max_count: u32,
    search: String,
    password: String,
}

fn part_1(password_records: &Vec<PasswordRecord>) -> u32 {
    let mut correct_passwords: u32 = 0;

    for password_record in password_records.iter() {
        let count = password_record
            .password
            .chars()
            .filter(|x| x.to_string() == password_record.search)
            .count();

        if count as u32 >= password_record.min_count && count as u32 <= password_record.max_count {
            correct_passwords += 1;
        }
    }

    correct_passwords
}

fn part_2(password_records: &Vec<PasswordRecord>) -> u32 {
    let mut correct_passwords: u32 = 0;

    for password_record in password_records.iter() {
        let first_character = password_record
            .password
            .chars()
            .nth(password_record.min_count as usize - 1)
            .unwrap();
        let second_character = password_record
            .password
            .chars()
            .nth(password_record.max_count as usize - 1)
            .unwrap();

        if first_character != second_character
            && (first_character.to_string() == password_record.search
                || second_character.to_string() == password_record.search)
        {
            correct_passwords += 1;
        }
    }

    correct_passwords
}

fn main() {
    let file = File::open("src/input/day2.txt").unwrap();
    let reader = BufReader::new(file);

    let password_records: Vec<PasswordRecord> = reader
        .lines()
        .into_iter()
        .map(|value| {
            let string = value.unwrap();
            let string_parts: Vec<&str> = string.split(' ').collect();
            let range: Vec<u32> = String::from_str(string_parts[0])
                .unwrap()
                .split('-')
                .into_iter()
                .map(|v| v.parse::<u32>().unwrap())
                .collect();
            let mut search: String = String::from(string_parts[1]);
            search.pop();

            PasswordRecord {
                min_count: range[0],
                max_count: range[1],
                search: search,
                password: String::from(string_parts[2]),
            }
        })
        .collect();

    println!("Part 1: {}", part_1(&password_records));
    println!("Part 2: {}", part_2(&password_records));
}

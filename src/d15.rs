use itertools::Itertools;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day15.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader.lines().into_iter().map(|x| x.unwrap()).collect();
    let numbers: Vec<isize> = lines[0]
        .split(',')
        .into_iter()
        .map(|x| x.parse::<isize>().unwrap())
        .collect();

    println!("Part 1: {}", part_1(&numbers));
    println!("Part 2: {}", part_2(&numbers));
}

fn part_1(numbers: &Vec<isize>) -> isize {
    let mut numbers_clone: Vec<isize> = numbers.clone();

    while numbers_clone.len() != 2020 {
        let last_position: Option<(usize, &isize)> = numbers_clone
            .iter()
            .rev()
            .dropping(1)
            .find_position(|n| *n == numbers_clone.last().unwrap());
        match last_position {
            Some((position, _)) => numbers_clone.push((position + 1) as isize),
            None => numbers_clone.push(0),
        }
    }

    *numbers_clone.last().unwrap()
}

fn part_2(numbers: &Vec<isize>) -> usize {
    let mut turn = numbers.len();
    let mut last_number: usize = *numbers.last().unwrap() as usize;
    let mut vec_map: Vec<usize> = vec![usize::MAX; 30000000];

    for (k, v) in numbers.iter().enumerate() {
        vec_map[*v as usize] = k + 1;
    }
    vec_map[last_number] = usize::MAX;

    while turn != 30000000 {
        turn += 1;
        let last_seen = vec_map[last_number];
        vec_map[last_number] = turn - 1;

        if last_seen == usize::MAX {
            //haven't seen this number before
            last_number = 0;
        } else {
            //have seen this number before
            last_number = turn - last_seen - 1;
        }
    }

    last_number
}

use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day14.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader.lines().into_iter().map(|x| x.unwrap()).collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

fn part_1(lines: &Vec<String>) -> usize {
    let mut memory: HashMap<usize, usize> = HashMap::new();
    let mut mask: String = String::from("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

    for line in lines {
        if &line[0..4] == "mask" {
            let split: Vec<&str> = line.split(" = ").collect();
            mask = split[1].to_owned();
        } else {
            let split: Vec<&str> = line.split(" = ").collect();
            let opening_bracket_id: usize = split[0].find('[').unwrap();
            let closing_bracket_id: usize = split[0].find(']').unwrap();

            let memory_address: usize = split[0][opening_bracket_id + 1..closing_bracket_id]
                .parse::<usize>()
                .unwrap();
            let mut int_value: usize = split[1].parse::<usize>().unwrap();
            let mut binary_value: String = format!("{:b}", int_value);
            //padding with 0s
            for _ in 0..36 - binary_value.len() {
                binary_value.insert(0, '0');
            }
            let mut masked_value: String = String::new();

            //get masked value
            for i in 0..36 {
                if mask.chars().nth(i).unwrap() == 'X' {
                    masked_value += &binary_value.chars().nth(i).unwrap().to_string();
                } else {
                    masked_value += &mask.chars().nth(i).unwrap().to_string();
                }
            }

            //write to memory
            int_value = usize::from_str_radix(masked_value.as_str(), 2).unwrap();
            memory.insert(memory_address, int_value);
        }
    }

    memory.values().into_iter().sum()
}

fn part_2(lines: &Vec<String>) -> usize {
    let mut memory: HashMap<usize, usize> = HashMap::new();
    let mut mask: String = String::from("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

    for line in lines {
        if &line[0..4] == "mask" {
            let split: Vec<&str> = line.split(" = ").collect();
            mask = split[1].to_owned();
        } else {
            let split: Vec<&str> = line.split(" = ").collect();
            let opening_bracket_id: usize = split[0].find('[').unwrap();
            let closing_bracket_id: usize = split[0].find(']').unwrap();

            let memory_address: usize = split[0][opening_bracket_id + 1..closing_bracket_id]
                .parse::<usize>()
                .unwrap();
            let int_value: usize = split[1].parse::<usize>().unwrap();
            let mut binary_value: String = format!("{:b}", memory_address);
            //padding with 0s
            for _ in 0..36 - binary_value.len() {
                binary_value.insert(0, '0');
            }
            let mut masked_value: String = String::new();
            let mut floating_bits: Vec<usize> = Vec::new();

            //get masked memory location value
            for i in 0..36 {
                if mask.chars().nth(i).unwrap() == 'X' {
                    floating_bits.push(i);
                    masked_value += "X";
                } else if mask.chars().nth(i).unwrap() == '1' {
                    masked_value += "1";
                } else {
                    masked_value += &binary_value.chars().nth(i).unwrap().to_string();
                }
            }

            //get a list of memory addresses after processing the floating bits
            let mut masked_memory_addresses: Vec<String> = vec![masked_value];
            
            for floating_bit in floating_bits {
                let mut new_addresses: Vec<String> = Vec::new();

                for masked_memory_address in &masked_memory_addresses {
                    let mut new_address_0: String = String::new();
                    let mut new_address_1: String = String::new();
                    for (id, v) in masked_memory_address.chars().enumerate() {
                        if id == floating_bit {
                            new_address_0.push('0'); 
                            new_address_1.push('1');
                        } else {
                            new_address_0.push(v);
                            new_address_1.push(v);
                        }
                    }
                    new_addresses.push(new_address_0);
                    new_addresses.push(new_address_1);
                }

                masked_memory_addresses = new_addresses;
            }

            //write to memory
            for masked_memory_address in masked_memory_addresses {
                memory.insert(usize::from_str_radix(&masked_memory_address, 2).unwrap(), int_value);
            }
        }
    }

    memory.values().into_iter().sum()
}

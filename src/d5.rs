use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day5.txt").unwrap();
    let reader = BufReader::new(file);

    let passes: Vec<String> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap())
        .collect();

    println!("Part 1: {}", part_1(&passes));
    println!("Part 2: {}", part_2(&passes));
}

fn part_1(passes: &Vec<String>) -> u32 {
    let mut max_id = 0;

    for pass in passes.iter() {
        max_id = std::cmp::max(max_id, calculate_seat_id(&pass));
    }

    max_id
}

fn part_2(passes: &Vec<String>) -> u32 {
    let mut seat_ids: Vec<u32> = passes.iter().map(|pass| calculate_seat_id(&pass)).collect();
    seat_ids.sort();

    let mut my_seat = 0;

    for seat_id in seat_ids[0]..=seat_ids[seat_ids.len() - 1] {
        if !seat_ids.contains(&seat_id) {
            my_seat = seat_id;
            break;
        }
    }

    my_seat
}

fn calculate_seat_id(passport: &String) -> u32 {
    let mut coded_passport = String::new();

    for character in passport.chars().into_iter() {
        match character {
            'B' | 'R' => coded_passport += "1",
            'F' | 'L' => coded_passport += "0",
            _ => panic!("Wrong passport!"),
        }
    }

    let row = u32::from_str_radix(&coded_passport[0..7], 2).unwrap();
    let column: u32 = u32::from_str_radix(&coded_passport[7..10], 2).unwrap();

    row * 8 + column
}
